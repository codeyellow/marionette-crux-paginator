const rollup = require('rollup');
const jst = require('rollup-plugin-jst');

rollup.rollup({
    entry: 'src/index.js',
    external: [
        'marionette',
        'underscore',
    ],
    plugins: [
        jst(),
    ],
}).then(function (bundle) {
    bundle.write({
        format: 'cjs',
        moduleName: 'marionette-crux-paginator',
        dest: 'dist/marionette-crux-paginator.js',
    });
}).catch(function (err) {
    console.log(String(err));
    process.exit(1);
});
