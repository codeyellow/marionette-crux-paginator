# marionette-crux-paginator

Implements a view for pagination. Only works with backbone-crux `2.0.7+` and backbone.paginator `2.0.2+`.

## Install

First make sure you have `underscore`, `marionette` and `backbone-crux` as a dependency.

Now add the view to a `LayoutView`:

```js
var Marionette = require('marionette');
var VPaginator = require('marionette-crux-paginator');
var _ = require('underscore');

module.exports = Marionette.LayoutView.extend({
    regions: {
        paginator: '._paginator-region'
    }
    onRender: function () {
        this.paginator.show(new VPaginator({
            collection: this.collection,
            previousText: 'Vorige' // Optional (default: 'Previous')
            nextText: 'Volgende' // Option (default: 'Next')
        }));
    }
});
```

New: You can also pass `displayPageSizeSelector` as an option to add a control for the page size.

## Example of CSS

```css
.pagination-wrapper {
    padding: 20px 0 10px;
    display: flex;
}

.pagination-filler {
    flex: 1;
}

.pagination-page-chooser-wrapper {
    flex: 1;
    text-align: center;
}

.pagination-page-size-chooser {
    margin-left: auto;
    text-align: right;
    flex: 1;
}

.pagination-previous {
    margin-right: 20px;
}

.pagination-next {
    margin-left: 20px;
}
```
