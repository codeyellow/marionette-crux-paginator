import Marionette from 'marionette';
import _ from 'underscore';
import template from './template.html';

export default Marionette.ItemView.extend({
    template: template,
    ui: {
        pagination: '.pagination-page-chooser-wrapper',
        nextPageButton: '._pagination-get-next-page',
        previousPageButton: '._pagination-get-previous-page',
        currentPageInput: '._pagination-current-page',
        totalRecordsText: '._pagination-total-records',
        totalPagesText: '._pagination-total-pages',
        loadingText: '._pagination-loading',
        pageSize: '._pagination-per-page',
    },
    events: {
        'click @ui.nextPageButton': 'getNextPage',
        'click @ui.previousPageButton': 'getPreviousPage',
        'input @ui.currentPageInput': 'getSpecificPage',
        'change @ui.pageSize': 'setPageSize',
    },
    collectionEvents: {
        'after:read:success': 'renderPaginator'
    },
    onRender: function () {
        this.hideLoading();
        this.renderPaginator();
    },
    templateHelpers: function () {
        return {
            displayPageSizeSelector: this.options.displayPageSizeSelector,
            previousText: this.options.previousText || 'Previous',
            nextText: this.options.nextText || 'Next',
        };
    },
    renderPaginator: function () {
        this.ui.pageSize.val(this.collection.state.pageSize);

        if (!this.collection.hasNextPage() && !this.collection.hasPreviousPage()) {
            return this.ui.pagination.addClass('hide');
        }

        this.ui.pagination.removeClass('hide');

        this.ui.nextPageButton.toggleClass('disabled', !this.collection.hasNextPage());
        this.ui.previousPageButton.toggleClass('disabled', !this.collection.hasPreviousPage());

        this.ui.totalPagesText.text(this.collection.state.totalPages);
        this.ui.currentPageInput.val(this.collection.state.currentPage);
        this.ui.currentPageInput.attr('max', this.collection.state.totalPages);
    },
    renderLoading: function (xhr) {
        this.showLoading();

        xhr.always(this.hideLoading.bind(this));
    },
    getPreviousPage: function (e) {
        e.preventDefault();

        this.renderLoading(this.collection.getPreviousPage());
        this.renderPaginator();
    },
    getNextPage: function (e) {
        e.preventDefault();

        this.renderLoading(this.collection.getNextPage());
        this.renderPaginator();
    },
    getSpecificPage: function () {
        this.getSpecificPageDebounced();
    },
    setPageSize: function () {
        var pageSize = parseInt(this.ui.pageSize.val());
        this.collection.setPageSize(pageSize);
    },
    getSpecificPageDebounced: _.debounce(function () {
        var pageNumber = parseInt(this.ui.currentPageInput.val());

        if (pageNumber > 0 && pageNumber <= this.collection.state.totalPages) {
            if (this.collection.xhr) {
                this.collection.xhr.abort();
            }

            this.renderLoading(this.collection.getPage(pageNumber));
        }
    }, 300),
    showLoading: function () {
        this.ui.loadingText.addClass('show');
    },
    hideLoading: function () {
        this.ui.loadingText.removeClass('show');
    },
});
