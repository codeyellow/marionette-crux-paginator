'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Marionette = _interopDefault(require('marionette'));
var _ = _interopDefault(require('underscore'));

function template(data) {
var __t, __p = '', __e = _, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
__p += '<div class="pagination-wrapper">\n    ';
 if (data.displayPageSizeSelector) { ;
__p += '\n    <div class="pagination-filler"></div>\n    ';
 } ;
__p += '\n    <div class="pagination-page-chooser-wrapper">\n        <a href="#" class="_pagination-get-previous-page button pagination-previous">' +
__e(data.previousText) +
'</a>\n        <input type="number" class="_pagination-current-page" min="1"> /\n        <span class="_pagination-total-pages"></span>\n        <a href="#" class="_pagination-get-next-page button pagination-next">' +
__e(data.nextText) +
'</a>\n        <div class="_pagination-loading pagination-loading loading-spinner show">\n            <div class="spinner-icon"></div>\n        </div>\n    </div>\n    ';
 if (data.displayPageSizeSelector) { ;
__p += '\n    <div class="pagination-page-size-chooser">\n        <select class="_pagination-per-page">\n            <option value="25">25</option>\n            <option value="50">50</option>\n            <option value="100">100</option>\n            <option value="200">200</option>\n        </select>\n        / page\n    </div>\n    ';
 } ;
__p += '\n</div>\n';
return __p
}

var index = Marionette.ItemView.extend({
    template: template,
    ui: {
        pagination: '.pagination-page-chooser-wrapper',
        nextPageButton: '._pagination-get-next-page',
        previousPageButton: '._pagination-get-previous-page',
        currentPageInput: '._pagination-current-page',
        totalRecordsText: '._pagination-total-records',
        totalPagesText: '._pagination-total-pages',
        loadingText: '._pagination-loading',
        pageSize: '._pagination-per-page',
    },
    events: {
        'click @ui.nextPageButton': 'getNextPage',
        'click @ui.previousPageButton': 'getPreviousPage',
        'input @ui.currentPageInput': 'getSpecificPage',
        'change @ui.pageSize': 'setPageSize',
    },
    collectionEvents: {
        'after:read:success': 'renderPaginator'
    },
    onRender: function () {
        this.hideLoading();
        this.renderPaginator();
    },
    templateHelpers: function () {
        return {
            displayPageSizeSelector: this.options.displayPageSizeSelector,
            previousText: this.options.previousText || 'Previous',
            nextText: this.options.nextText || 'Next',
        };
    },
    renderPaginator: function () {
        this.ui.pageSize.val(this.collection.state.pageSize);

        if (!this.collection.hasNextPage() && !this.collection.hasPreviousPage()) {
            return this.ui.pagination.addClass('hide');
        }

        this.ui.pagination.removeClass('hide');

        this.ui.nextPageButton.toggleClass('disabled', !this.collection.hasNextPage());
        this.ui.previousPageButton.toggleClass('disabled', !this.collection.hasPreviousPage());

        this.ui.totalPagesText.text(this.collection.state.totalPages);
        this.ui.currentPageInput.val(this.collection.state.currentPage);
        this.ui.currentPageInput.attr('max', this.collection.state.totalPages);
    },
    renderLoading: function (xhr) {
        this.showLoading();

        xhr.always(this.hideLoading.bind(this));
    },
    getPreviousPage: function (e) {
        e.preventDefault();

        this.renderLoading(this.collection.getPreviousPage());
        this.renderPaginator();
    },
    getNextPage: function (e) {
        e.preventDefault();

        this.renderLoading(this.collection.getNextPage());
        this.renderPaginator();
    },
    getSpecificPage: function () {
        this.getSpecificPageDebounced();
    },
    setPageSize: function () {
        var pageSize = parseInt(this.ui.pageSize.val());
        this.collection.setPageSize(pageSize);
    },
    getSpecificPageDebounced: _.debounce(function () {
        var pageNumber = parseInt(this.ui.currentPageInput.val());

        if (pageNumber > 0 && pageNumber <= this.collection.state.totalPages) {
            if (this.collection.xhr) {
                this.collection.xhr.abort();
            }

            this.renderLoading(this.collection.getPage(pageNumber));
        }
    }, 300),
    showLoading: function () {
        this.ui.loadingText.addClass('show');
    },
    hideLoading: function () {
        this.ui.loadingText.removeClass('show');
    },
});

module.exports = index;